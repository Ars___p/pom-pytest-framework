## 🙌🏻 Вступление


Этот репозиторий содержит базовый пример использования PageObject
с Selenium и Python (PyTest + Selenium).

## 🛠️ Запуск проекта:

1. Клонировать проект
2. Установить и запустить виртуальную среду
    ```bash
    python3 -m venv pom-pytest-framework

    pom-pytest-framework/Scripts/activate
    ```
3. Установить зависимости из requirements.txt
    ```bash
    pip3 install -r requirements
    ```
4. Запустить тесты
    ```bash
    pytest -v
    ```


