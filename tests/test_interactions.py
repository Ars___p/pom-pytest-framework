import time

import allure

from pages.interactions_page import SortablePage, SelectablePage, ResizablePage, DroppablePage


@allure.suite('Interactions')
class TestInteractions:
    @allure.feature('Страница "Sortable"')
    class TestSortablePage:
        @allure.title('Проверка изменений в сортирующемся списке')
        def test_sortable_list(self, get_driver):
            page = SortablePage(get_driver)
            list_before, list_after = page.change_list_order()
            with allure.step('Проверить результат'):
                assert list_before != list_after, 'Порядок в списке не изменнился'

        @allure.title('Проверка изменений в сортируейся таблице')
        def test_sortable_grid(self, get_driver):
            page = SortablePage(get_driver)
            grid_before, grid_after = page.change_grid_order()
            with allure.step('Проверить результат'):
                assert grid_before != grid_after, 'Порядок в таблице не изменнился'

    @allure.feature('Страница "Selectable"')
    class TestSelectablePage:
        @allure.title('Проверка выделения элементов списка')
        def test_selectable_list(self, get_driver):
            page = SelectablePage(get_driver)
            item_list = page.select_list_item()
            with allure.step('Проверить результат'):
                assert len(item_list) > 0, "Элементы списка не выделены"

        @allure.title('Проверка выделения элементов таблицы')
        def test_selectable_grid(self, get_driver):
            page = SelectablePage(get_driver)
            item_grid = page.select_grid_item()
            with allure.step('Проверить результат'):
                assert len(item_grid) > 0, "Элементы таблицы не выделены"

    @allure.feature('Страница "Resizable"')
    class TestResizablePage:
        @allure.title('Проверка изменения размера поля c границами')
        def test_resizable_box(self, get_driver):
            page = ResizablePage(get_driver)
            max_box, min_box = page.change_size_resizable_box()
            with allure.step('Проверить результат'):
                assert min_box < max_box, "maximum size not equal to '500px', '300px'"

        @allure.title('Проверка изменения размера поля без границ')
        def test_resizable(self, get_driver):
            page = ResizablePage(get_driver)
            max_resize, min_resize = page.change_size_resizable()
            with allure.step('Проверить результат'):
                assert min_resize < max_resize, "Размер не был изменен"


    @allure.feature('Страница "Droppable"')
    class TestDroppablePage:
        @allure.title('Проверка простого перетаскивания')
        def test_simple_droppable(self, get_driver):
            page = DroppablePage(get_driver)
            page.simple_tab.click()
            page.drag_me_simple.drag_and_drop_to_element(page.drop_here_simple)
            text = page.drop_here_simple.get_text()
            with allure.step('Проверить результат'):
                assert text == 'Dropped!', "элемент не был помещен в поле"

        @allure.title('Проверка подтвержаемого перетаскивания')
        def test_accept_droppable(self, get_driver):
            page = DroppablePage(get_driver)
            page.accept_tab.click()
            page.acceptable.drag_and_drop_to_element(page.drop_here_accept)
            accept = page.drop_here_accept.get_text()
            with allure.step('Проверить результат'):
                assert accept == 'Dropped!', "элемент не был подтвержден"

        @allure.title('Проверка не подтвержаемого перетаскивания')
        def test_not_accept_droppable(self, get_driver):
            page = DroppablePage(get_driver)
            page.accept_tab.click()
            page.not_acceptable.drag_and_drop_to_element(page.drop_here_accept)
            not_accept = page.drop_here_accept.get_text()
            with allure.step('Проверить результат'):
                assert not_accept == 'Drop here', "элемент был подтвержден"

        @allure.title('Проверка "не жадных" элементов')
        def test_prevent_not_greegy(self, get_driver):
            page = DroppablePage(get_driver)
            page.prevent_tab.click()
            page.drag_me_prevent.drag_and_drop_to_element(page.not_greedy_inner_box)
            not_greedy = page.not_greedy_drop_box_text.get_text()
            with allure.step('Проверить результат'):
                assert not_greedy == 'Dropped!', "Текст не был изменен"

        @allure.title('Проверка "жадных" элементов')
        def test_prevent_greegy(self, get_driver):
            page = DroppablePage(get_driver)
            page.prevent_tab.click()
            page.drag_me_prevent.drag_and_drop_to_element(page.greedy_inner_box)
            greedy = page.not_greedy_drop_box_text.get_text()
            with allure.step('Проверить результат'):
                assert greedy == 'Outer droppable', "Текст был изменен"

        @allure.title('Проверка возвращающегося элемента')
        def test_will_revert_draggable_droppable(self, get_driver):
            page = DroppablePage(get_driver)
            page.revert_tab.click()
            page.will_revert.drag_and_drop_by_offset(10, 10)
            before = page.will_revert.get_attribute('style')
            page.will_revert.drag_and_drop_to_element(page.drop_here_revert)
            time.sleep(0.5)
            after = page.will_revert.get_attribute('style')
            with allure.step('Проверить результат'):
                assert before == after, 'элемент не вернулся на исходную позицию'

        @allure.title('Проверка не возвращающегося элемента')
        def test_not_revert_draggable_droppable(self, get_driver):
            page = DroppablePage(get_driver)
            page.revert_tab.click()
            before = page.not_revert.get_attribute('style')
            page.not_revert.drag_and_drop_to_element(page.drop_here_revert)
            time.sleep(0.5)
            after = page.not_revert.get_attribute('style')
            with allure.step('Проверить результат'):
                assert before != after, 'элемент вернулся на исходную позицию'