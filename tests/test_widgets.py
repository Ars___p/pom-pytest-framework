import random
import time

import allure

from pages.widgets_page import AccordianPage, AutoCompletePage, SliderPage, ProgressBarPage, TabsPage, ToolTipsPage


@allure.suite('Widgets')
class TestWidgets:
    @allure.feature('Страница "Accordian"')
    class TestAccordianPage:

        @allure.title('Проверка виджета аккордион')
        def test_accordian(self, get_driver):
            page = AccordianPage(get_driver)
            third_title, third_content = page.check_accordian('third')
            second_title, second_content = page.check_accordian('second')
            first_title, first_content = page.check_accordian('first')
            with allure.step('Проверить результат'):
                assert first_title == 'What is Lorem Ipsum?' and first_content > 0, 'Некорректный заголовок или текст утерян'
                assert second_title == 'Where does it come from?' and second_content > 0, 'Некорректный заголовок или текст утерян'
                assert third_title == 'Why do we use it?' and third_content > 0, 'Некорректный заголовок или текст утерян'

    @allure.feature('Страница "Autocomplete"')
    class TestAutoCompletePage:
        @allure.title('Проверка автозаполнения')
        def test_fill_multi_autocomplete(self, get_driver):
            page = AutoCompletePage(get_driver)
            colors = page.fill_input_multi()
            colors_result = page.check_color_in_multi()
            with allure.step('Проверить результат'):
                assert colors == colors_result, 'Добавленные цвета не введены'

        @allure.title('Проверка удаления элементов из поля с автозаполнением')
        def test_remove_value_from_multi(self, get_driver):
            page = AutoCompletePage(get_driver)
            page.fill_input_multi()
            count_value_before, count_value_after = page.remove_value_from_multi()
            with allure.step('Проверить результат'):
                assert count_value_before != count_value_after, "значение не было удалено"

        @allure.title('Проверка автозаполнения в поле с одним значением')
        def test_fill_single_autocomplete(self, get_driver):
            page = AutoCompletePage(get_driver)
            color = page.fill_input_single()
            color_result = page.check_color_in_single()
            with allure.step('Проверить результат'):
                assert color == color_result, 'Добавленные цвета не введены'

    @allure.feature('Страница "Slider"')
    class TestSliderPage:
        @allure.title('Проверка слайдера')
        def test_slider(self, get_driver):
            page = SliderPage(get_driver)
            value_before = page.slider_value.get_attribute('value')
            page.input_slider.click(1, random.randint(1, 100), 0)
            value_after = page.slider_value.get_attribute('value')
            with allure.step('Проверить результат'):
                assert value_before != value_after, 'Значение слайдера не было изменено'

    @allure.feature('Страница "Progress Bar"')
    class TestProgressBarPage:
        @allure.title('Проверка прогресс бара')
        def test_progress_bar(self, get_driver):
            page = ProgressBarPage(get_driver)
            value_before = page.progress_bar_value.get_text()
            page.progress_bar_button.click()
            time.sleep(random.randint(4, 6))
            page.progress_bar_button.click()
            value_after = page.progress_bar_value.get_text()
            with allure.step('Проверить результат'):
                assert value_before != value_after, 'Прогресс бар не был изменен'

    @allure.feature('Страница "Tabs"')
    class TestTabsPage:
        @allure.title('Проверка табов')
        def test_tabs(self, get_driver):
            tabs = TabsPage(get_driver)
            what_button, what_content = tabs.check_tabs('what')
            origin_button, origin_content = tabs.check_tabs('origin')
            use_button, use_content = tabs.check_tabs('use')
            more_button, more_content = tabs.check_tabs('more')
            with allure.step('Проверить результат'):
                assert what_button == 'What' and what_content != 0, 'Таб "What" не открывается или отображается некорректно'
                assert origin_button == 'Origin' and origin_content != 0, 'Таб "Origin" не открывается или отображается некорректно'
                assert use_button == 'Use' and use_content != 0, 'Таб "Use" не открывается или отображается некорректно'
                assert more_button == 'More' and what_content != 0, 'Таб "More" не открывается или отображается некорректно'

    @allure.feature('Страница "Tool Tips"')
    class TestToolTips:
        @allure.title('Проверка тултипов')
        def test_tool_tips(self, get_driver):
            page = ToolTipsPage(get_driver)
            button_text, field_text, contrary_text, section_text = page.check_tool_tips()
            with allure.step('Проверить результат'):
                assert button_text == 'You hovered over the Button', 'Ховер не отображается или текст некорректный'
                assert field_text == 'You hovered over the text field', 'Ховер не отображается или текст некорректный'
                assert contrary_text == 'You hovered over the Contrary', 'Ховер не отображается или текст некорректный'
                assert section_text == 'You hovered over the 1.10.32', 'Ховер не отображается или текст некорректный'
