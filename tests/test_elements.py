import time

import allure

from pages.elements_page import TextBoxPage, CheckBoxPage, RadioButtonPage, WebTablePage, generate_person, ButtonsPage, \
    LinksPage, DynamicPropertiesPage


@allure.suite('Страница "Elements"')
class TestElements:
    @allure.feature('Textbox')
    class TestTextbox:

        @allure.title('Проверка заполнения текстовых полей')
        def test_text_box(self, get_driver):
            page = TextBoxPage(get_driver)
            first_name, last_name, full_name, email, age, salary, department, current_address, permanent_address \
                = generate_person()
            page.full_name.send_keys(full_name)
            page.email.send_keys(email)
            page.current_address.send_keys(current_address)
            page.permanent_address.scroll_to_element()
            page.permanent_address.send_keys(permanent_address)
            page.submit.click()
            with allure.step('Проверить результат'):
                assert page.created_full_name.get_text().split(
                    ':')[1] == full_name, 'Значения full_name не совпадают'
                assert page.created_email.get_text().split(
                    ':')[1] == email, 'Значения email не совпадают'
                assert page.created_current_address.get_text().split(
                    ':')[1] == current_address, 'Значения current_address не совпадают'
                assert page.created_permanent_address.get_text().split(
                    ':')[1] == permanent_address, 'Значения permanent_address не совпадают'

    @allure.feature('CheckBox')
    class TestCheckBox:

        @allure.title('Проверка чекбоксов')
        def test_check_box(self, get_driver):
            page = CheckBoxPage(get_driver)
            page.expand_all.click()
            page.item_list.get_text_of_all_elements()
            page.click_random_checkbox()
            page.checked_list.find_all()
            page.result_list.find_all()
            input_checkbox = page.get_checked_checkboxes()
            output_result = page.get_output_result()
            with allure.step('Проверить результат'):
                assert input_checkbox == output_result, 'Выделенные и отображающиеся чекбоксы не совпадают'

    @allure.feature('RadioButton')
    class TestRadioButton:

        @allure.title('Проверка радиокнопки')
        def test_radio_button(self, get_driver):
            page = RadioButtonPage(get_driver)
            page.yes_radiobutton.click()
            output_yes = page.output.get_text()
            page.impressive_radiobutton.click()
            output_impressive = page.output.get_text()
            page.no_radiobutton.click()
            output_no = page.output.get_text()
            with allure.step('Проверить результат'):
                assert output_yes == 'Yes', "'Yes' не был выбран"
                assert output_impressive == 'Impressive', "'Impressive' не был выбран"
                assert output_no == "No", "'No' не был выбран"

    @allure.feature('WebTable')
    class TestWebTable:

        @allure.title('Добавление нового человека в таблицу')
        def test_web_table_add_person(self, get_driver):
            page = WebTablePage(get_driver)
            page.add_button.click()
            first_name, last_name, full_name, email, age, salary, department, current_address, permanent_address \
                = generate_person()
            page.first_name_input.send_keys(first_name)
            page.last_name_input.send_keys(last_name)
            page.email_input.send_keys(email)
            page.age_input.send_keys(age)
            page.salary_input.send_keys(salary)
            page.department_input.send_keys(department)
            page.submit_button.click()
            page.full_list.get_text_of_all_elements()
            table_result = page.check_new_added_person()
            with allure.step('Проверить результат'):
                assert [first_name, last_name, str(age),
                        email, str(salary), department] in table_result, "Человек не был добавлен в таблицу"

        @allure.title('Поиск человека в таблице')
        def test_web_table_search_person(self, get_driver):
            page = WebTablePage(get_driver)
            page.add_button.click()
            first_name, last_name, full_name, email, age, salary, department, current_address, permanent_address \
                = generate_person()
            page.first_name_input.send_keys(first_name)
            page.last_name_input.send_keys(last_name)
            page.email_input.send_keys(email)
            page.age_input.send_keys(age)
            page.salary_input.send_keys(salary)
            page.department_input.send_keys(department)
            page.submit_button.click()
            key_word = first_name
            page.search_input.send_keys(key_word)
            page.delete_button.find()
            table_result = page.check_search_person()
            with allure.step('Проверить результат'):
                assert key_word in table_result, "Человек не был найден в таблице"

        @allure.title('Обновление информации о человека в таблице')
        def test_web_table_update_person_info(self, get_driver):
            page = WebTablePage(get_driver)
            page.add_button.click()
            first_name, last_name, full_name, email, age, salary, department, current_address, permanent_address \
                = generate_person()
            page.first_name_input.send_keys(first_name)
            page.last_name_input.send_keys(last_name)
            page.email_input.send_keys(email)
            page.age_input.send_keys(age)
            page.salary_input.send_keys(salary)
            page.department_input.send_keys(department)
            page.submit_button.click()
            page.search_input.send_keys(last_name)
            page.update_button.click()
            department = page.update_person_department()
            page.submit_button.click()
            page.delete_button.find()
            row = page.check_search_person()
            with allure.step('Проверить результат'):
                assert department in row, "Карточка не была изменена"

        @allure.title('Удаление человека из таблицы')
        def test_web_table_delete_person(self, get_driver):
            page = WebTablePage(get_driver)
            page.add_button.click()
            first_name, last_name, full_name, email, age, salary, department, current_address, permanent_address \
                = generate_person()
            page.first_name_input.send_keys(first_name)
            page.last_name_input.send_keys(last_name)
            page.email_input.send_keys(email)
            page.age_input.send_keys(age)
            page.salary_input.send_keys(salary)
            page.department_input.send_keys(department)
            page.submit_button.click()
            page.search_input.send_keys(email)
            page.delete_button.click()
            text = page.no_rows_found.get_text()
            with allure.step('Проверить результат'):
                assert text == "No rows found", 'Карточка не была удалена'

    @allure.feature('Buttons')
    class TestButtonsPage:

        @allure.title('Проверка кликов по разным кнопкам')
        def test_click_button(self, get_driver):
            page = ButtonsPage(get_driver)
            page.double_click_button.double_click()
            page.right_click_button.right_mouse_click()
            page.click_me_button.click()
            with allure.step('Проверить результат'):
                assert page.double_click_message.is_visible(), 'Двойной клик по кнопке не был произведен'
                assert page.right_click_message.is_visible(), 'Клик правой копки мыши по копке не был произведен'
                assert page.click_me_message.is_visible(), 'Клик по кнопке не был произведн'

    @allure.feature('Links')
    class TestLinksPage:

        @allure.title('Проверка валидной ссылки')
        def test_check_valid_link(self, get_driver):
            page = LinksPage(get_driver)
            status_code, current_url, link_href = page.check_link('valid')
            with allure.step('Проверить результат'):
                assert status_code == 200
                assert current_url == link_href, "Ссылка сломана или некорректный url"

        @allure.title('Проверка сломанной ссылки')
        def test_check_broken_link(self, get_driver):
            page = LinksPage(get_driver)
            status_code, current_url, link_href = page.check_link('broken')
            with allure.step('Проверить результат'):
                assert status_code == 500, 'Изображение сломано или некорректный статус код'

        @allure.title('Проверка валидного изображения')
        def test_check_valid_image(self, get_driver):
            page = LinksPage(get_driver)
            status_code = page.check_image('valid')
            with allure.step('Проверить результат'):
                assert status_code == 200, 'Не корректный статус код'

        @allure.title('Проверка сломанного изображения')
        def test_check_broken_image(self, get_driver):
            page = LinksPage(get_driver)
            status_code = page.check_image('broken')
            with allure.step('Проверить результат'):
                assert status_code == 500, 'Не корректный статус код'

    @allure.feature('Dynamic Properties')
    class TestDynamicPropertiesPage:

        @allure.title('Проверка динамичческих свойств')
        def test_dynamic_properties(self, get_driver):
            page = DynamicPropertiesPage(get_driver)
            enable = page.check_enable_button()
            with allure.step('Проверить результат'):
                assert enable is True, 'Кнопка осталась не кликабельной'

        @allure.title('Проверка повляющейся кнопки')
        def test_appear_button(self, get_driver):
            page = DynamicPropertiesPage(get_driver)
            appear = page.check_appear_of_button()
            with allure.step('Проверить результат'):
                assert appear is True, 'Кнопка осталась невидимой'

        @allure.title('Проверка активации кнопки')
        def test_enable_button(self, get_driver):
            page = DynamicPropertiesPage(get_driver)
            color_before, color_after = page.check_changed_of_color()
            with allure.step('Проверить результат'):
                assert color_after != color_before, 'Цвет не был изменен'
