import allure

from pages.windows_page import BrowserWindowsPage, AlertsPage, FramesPage, ModalDialogsPage


@allure.suite('Страница "Alerts, Frame & Windows"')
class TestAlertsFrameWindow:
    @allure.feature('Browser Windows')
    class TestBrowserWindows:

        @allure.title('Проверка открытия новой вкладки')
        def test_new_tab(self, get_driver):
            page = BrowserWindowsPage(get_driver)
            text_result = page.check_opened_new_tab()
            with allure.step('Проверить результат'):
                assert text_result == 'This is a sample page', "Новая вкладка не была открыта"

        @allure.title('Проверка открытия нового окна')
        def test_new_window(self, get_driver):
            page = BrowserWindowsPage(get_driver)
            text_result = page.check_opened_new_window()
            with allure.step('Проверить результат'):
                assert text_result == 'This is a sample page', "Новое окно не было открыто"

    @allure.feature('Alerts Page')
    class TestAlertsPage:

        @allure.title('Проверка появления алерта')
        def test_see_alert(self, get_driver):
            page = AlertsPage(get_driver)
            alert_text = page.check_see_alert()
            with allure.step('Проверить результат'):
                assert alert_text == 'You clicked a button', "Алерт не появился"

        @allure.title('Проверка появления алерта после 5 секунд')
        def test_alert_appear_5_sec(self, get_driver):
            page = AlertsPage(get_driver)
            alert_text = page.check_alert_appear_5_sec()
            with allure.step('Проверить результат'):
                assert alert_text == 'This alert appeared after 5 seconds', "Алерт не появился"

        @allure.title('Проверка появления алерта с подтверждением')
        def test_confirm_alert(self, get_driver):
            page = AlertsPage(get_driver)
            alert_text = page.check_confirm_alert()
            with allure.step('Проверить результат'):
                assert alert_text == 'You selected Ok', "Алерт не появился"

        @allure.title('Проверка аллерта с текстовым полем')
        def test_prompt_alert(self, get_driver):
            page = AlertsPage(get_driver)
            text, alert_text = page.check_prompt_alert()
            with allure.step('Проверить результат'):
                assert text in alert_text, "Алерт не появился"

    @allure.feature('Frame Page')
    class TestFramesPage:
        @allure.title('Проверка страницы с фреймами')
        def test_frame_1(self, get_driver):
            page = FramesPage(get_driver)
            result_frame1 = page.check_frame('frame1')
            with allure.step('Проверить результат'):
                assert result_frame1 == ['500px', '350px'], 'Фрейм не существует'

        @allure.title('Проверка страницы с фреймами')
        def test_frame_2(self, get_driver):
            page = FramesPage(get_driver)
            result_frame2 = page.check_frame('frame2')
            with allure.step('Проверить результат'):
                assert result_frame2 == ['100px', '100px'], 'Фрейм не существует'

    @allure.feature('Modal Dialog Page')
    class TestModalDialogsPage:
        @allure.title('Проверка малого модального окна с текстом')
        def test_small_modal_dialog(self, get_driver):
            page = ModalDialogsPage(get_driver)
            title, body_length = page.check_modal_dialogs('small_modal')
            with allure.step('Проверить результат'):
                assert title == 'Small Modal', 'Заголовок не "Small modal"'
                assert body_length == 47, 'Некорректное количество символов в модальном окне'

        @allure.title('Проверка большого модального окна с текстом')
        def test_large_modal_dialog(self, get_driver):
            page = ModalDialogsPage(get_driver)
            title, body_length = page.check_modal_dialogs('large_modal')
            with allure.step('Проверить результат'):
                assert title == 'Large Modal', 'Заголовок не "Large modal"'
                assert body_length == 574, 'Некорректное количество символов в модальном окне'