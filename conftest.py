import uuid
import allure
import settings
import pytest

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options

driver = None
"""С помощью данных функций считываются данные из файла settings.py
    В нем можно выбрать какой браузер необходимо инициализировать при запуске тестов"""


def pytest_addoption(parser):
    parser.addoption("--browser", action='store', default=settings.browser)


@pytest.fixture
def get_browser(request):
    browser = request.config.getoption("--browser")
    return browser


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item):
    # Эта функция помогает получить статус теста
    # и в случае, если он упал отдать информацию в тирдаун:

    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep


@pytest.fixture
def get_driver(get_browser, request):
    global driver
    if get_browser == "chrome":
        chrome_options = Options()
        chrome_options.page_load_strategy = 'eager'  # Используй 'eager' если НЕ нужно дожидаться полной загрузки страницы
        driver = webdriver.Chrome(options=chrome_options)
        driver.maximize_window()
    elif get_browser == "firefox":
        firefox_options = webdriver.FirefoxOptions()
        driver = webdriver.Firefox(options=firefox_options)
    elif get_browser == "headless":
        options = Options()
        options.add_argument("--disable-extensions")  # Данные параметры
        # options.add_argument("--disable-gpu")  # могут использоваться
        options.add_argument("--no-sandbox")  # только на linux
        options.add_argument('--log-level=DEBUG')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--headless")
        options.add_argument("window-size=1920,1080")
        options.page_load_strategy = 'eager'
        driver = webdriver.Chrome(options=options)
    else:
        print("Браузер не поддерживается выбери 'chrome', 'firefox' или 'headless'")
    # Возвращаем экземпляр браузера тест-кейсу
    yield driver

    if request.node.rep_call.failed:
        # Сделать скришот если тест упал
        try:
            driver.save_screenshot('screenshots/' + str(uuid.uuid4()) + '.png')

            # Прикрепить скринот к аллюр отчету
            allure.attach(driver.get_screenshot_as_png(),
                          name=request.function.__name__,
                          attachment_type=allure.attachment_type.PNG)

        except:
            pass

    driver.quit()
