import random
import time

import allure
from selenium.common.exceptions import UnexpectedAlertPresentException
from pages.base_page import BasePage, BasePageElement


class BrowserWindowsPageLocators:
    NEW_TAB_BUTTON = BasePageElement(css_selector='button[id="tabButton"]')
    NEW_WINDOW_BUTTON = BasePageElement(css_selector='button[id="windowButton"]')
    TITLE_NEW = BasePageElement(css_selector='h1[id="sampleHeading"]')


class BrowserWindowsPage(BasePage):
    locators = BrowserWindowsPageLocators()
    new_tab_button = locators.NEW_TAB_BUTTON
    new_window_button = locators.NEW_WINDOW_BUTTON
    title_new = locators.TITLE_NEW

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/browser-windows'
        super().__init__(web_driver, url)

    @allure.step('Проверка открытия новой вкладки')
    def check_opened_new_tab(self):
        self.new_tab_button.click()
        self.switch_window(1)
        text_title = self.title_new.get_text()
        return text_title

    @allure.step('Проверка открытия нового окна')
    def check_opened_new_window(self):
        self.new_window_button.click()
        self.switch_window(1)
        text_title = self.title_new.get_text()
        return text_title


class AlertsPageLocators:
    SEE_ALERT_BUTTON = BasePageElement(css_selector='button[id="alertButton"]')
    APPEAR_ALERT_AFTER_5_SEC_BUTTON = BasePageElement(css_selector='button[id="timerAlertButton"]')
    CONFIRM_BOX_ALERT_BUTTON = BasePageElement(css_selector='button[id="confirmButton"]')
    CONFIRM_RESULT = BasePageElement(css_selector='span[id="confirmResult"]')
    PROMPT_BOX_ALERT_BUTTON = BasePageElement(css_selector='button[id="promtButton"]')
    PROMPT_RESULT = BasePageElement(css_selector='span[id="promptResult"]')


class AlertsPage(BasePage):
    locators = AlertsPageLocators()
    see_alert_button = locators.SEE_ALERT_BUTTON
    appear_alert_button = locators.APPEAR_ALERT_AFTER_5_SEC_BUTTON
    confirm_box_alert = locators.CONFIRM_BOX_ALERT_BUTTON
    confirm_result = locators.CONFIRM_RESULT
    prompt_box_alert = locators.PROMPT_BOX_ALERT_BUTTON
    prompt_result = locators.PROMPT_RESULT

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/alerts'
        super().__init__(web_driver, url)

    @allure.step('Получить текст из алерта')
    def check_see_alert(self):
        self.see_alert_button.click()
        alert_window = self._web_driver.switch_to.alert
        return alert_window.text

    @allure.step('Получение текста из аллерта, появляющегося после 5 секунд')
    def check_alert_appear_5_sec(self):
        self.appear_alert_button.click()
        time.sleep(6)
        try:
            alert_window = self._web_driver.switch_to.alert
            return alert_window.text
        except UnexpectedAlertPresentException:
            alert_window = self._web_driver.switch_to.alert
            return alert_window.text

    @allure.step('Проверка подтверждения в алерте')
    def check_confirm_alert(self):
        self.confirm_box_alert.click()
        alert_window = self._web_driver.switch_to.alert
        alert_window.accept()
        text_result = self.confirm_result.get_text()
        return text_result

    @allure.step('Проверка алерта с текствоым полем')
    def check_prompt_alert(self):
        text = f"test{random.randint(0, 999)}"
        self.prompt_box_alert.click()
        alert_window = self._web_driver.switch_to.alert
        alert_window.send_keys(text)
        alert_window.accept()
        text_result = self.prompt_result.get_text()
        return text, text_result


class FramesPageLocators:
    FIRST_FRAME = BasePageElement(css_selector='iframe[id="frame1"]')
    SECOND_FRAME = BasePageElement(css_selector='iframe[id="frame2"]')
    TITLE_FRAME = BasePageElement(css_selector='h1[id="sampleHeading"]')


class FramesPage(BasePage):
    locators = FramesPageLocators()
    first_frame = locators.FIRST_FRAME
    second_frame = locators.SECOND_FRAME
    title_frame = locators.TITLE_FRAME

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/frames'
        super().__init__(web_driver, url)

    @allure.step('Проверка фрейма')
    def check_frame(self, choice):
        choices = {'frame1': self.first_frame,
                   'frame2': self.second_frame}

        frame = choices[choice]
        width = frame.get_attribute('width')
        height = frame.get_attribute('height')
        return [width, height]


class ModalDialogsPageLocators:
    SMALL_MODAL_BUTTON = BasePageElement(css_selector='button[id="showSmallModal"]')
    BODY_SMALL_MODAL = BasePageElement(css_selector='div[class="modal-body"]')
    TITLE_SMALL_MODAL = BasePageElement(css_selector='div[id="example-modal-sizes-title-sm"]')
    SMALL_MODAL_CLOSE = BasePageElement(css_selector='button[id="closeSmallModal"]')

    LARGE_MODAL_BUTTON = BasePageElement(css_selector='button[id="showLargeModal"]')
    BODY_LARGE_MODAL = BasePageElement(css_selector='div[class="modal-body"] p')
    TITLE_LARGE_MODAL = BasePageElement(css_selector='div[id="example-modal-sizes-title-lg"]')
    LARGE_MODAL_CLOSE = BasePageElement(css_selector='button[id="closeLargeModal"]')


class ModalDialogsPage(BasePage):
    locators = ModalDialogsPageLocators()
    small_modal_button = locators.SMALL_MODAL_BUTTON
    body_small_modal = locators.BODY_SMALL_MODAL
    title_small_modal = locators.TITLE_SMALL_MODAL
    small_modal_close = locators.SMALL_MODAL_CLOSE
    large_modal_button = locators.LARGE_MODAL_BUTTON
    body_large_modal = locators.BODY_LARGE_MODAL
    title_large_modal = locators.TITLE_LARGE_MODAL
    large_modal_close = locators.LARGE_MODAL_CLOSE

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/modal-dialogs'
        super().__init__(web_driver, url)

    @allure.step('Проверка модального окна')
    def check_modal_dialogs(self, choice):
        if choice == 'small_modal':
            self.small_modal_button.click()
            self.small_modal_close.wait_to_be_clickable()
            title = self.title_small_modal.get_text()
            body = self.body_small_modal.get_text()
            return title, len(body)

        if choice == 'large_modal':
            self.large_modal_button.click()
            self.large_modal_close.wait_to_be_clickable()
            title = self.title_large_modal.get_text()
            body = self.body_large_modal.get_text()
            return title, len(body)
