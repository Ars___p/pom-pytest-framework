import time
import random
import allure

from generator.generator import generated_color
from pages.base_page import BasePage, BasePageElement, BasePageManyElements


class AccordianPageLocators:
    SECTION_FIRST = BasePageElement(css_selector='div[id="section1Heading"]')
    SECTION_CONTENT_FIRST = BasePageElement(css_selector='div[id="section1Content"] p')
    SECTION_SECOND = BasePageElement(css_selector='div[id="section2Heading"]')
    SECTION_CONTENT_SECOND = BasePageElement(css_selector='div[id="section2Content"] p')
    SECTION_THIRD = BasePageElement(css_selector='div[id="section3Heading"]')
    SECTION_CONTENT_THIRD = BasePageElement(css_selector='div[id="section3Content"] p')


class AccordianPage(BasePage):
    locators = AccordianPageLocators()
    section_first = locators.SECTION_FIRST
    section_content_first = locators.SECTION_CONTENT_FIRST
    section_second = locators.SECTION_SECOND
    section_content_second = locators.SECTION_CONTENT_SECOND
    section_third = locators.SECTION_THIRD
    section_content_third = locators.SECTION_CONTENT_THIRD

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/accordian'
        super().__init__(web_driver, url)

    @allure.step('check accordian widget')
    def check_accordian(self, accordian_num):
        accordian = {'first':
                         {'title': self.section_first,
                          'content': self.section_content_first},
                     'second':
                         {'title': self.section_second,
                          'content': self.section_content_second},
                     'third':
                         {'title': self.section_third,
                          'content': self.section_content_third},
                     }

        section_title = accordian[accordian_num]['title']
        section_title.wait_to_be_clickable()
        section_title.click()
        time.sleep(0.1)
        section_content = accordian[accordian_num]['content'].get_text()
        return section_title.get_text(), len(section_content)


class AutoCompletePageLocators:
    MULTI_INPUT = BasePageElement(css_selector='input[id="autoCompleteMultipleInput"]')
    MULTI_VALUE = BasePageManyElements(css_selector='div[class="css-1rhbuit-multiValue auto-complete__multi-value"]')
    MULTI_SELECTOR = BasePageManyElements(css_selector='div[id="react-select-2-option-0"]')
    MULTI_VALUE_REMOVE = BasePageManyElements(
        css_selector='div[class="css-1rhbuit-multiValue auto-complete__multi-value"] svg path')
    SINGLE_INPUT = BasePageElement(css_selector='input[id="autoCompleteSingleInput"]')
    SINGLE_VALUE = BasePageElement(css_selector='div[class="auto-complete__single-value css-1uccc91-singleValue"]')
    SINGLE_SELECTOR = BasePageElement(css_selector='div[id="react-select-3-option-0"]')


class AutoCompletePage(BasePage):
    locators = AutoCompletePageLocators()
    multiple_input = locators.MULTI_INPUT
    multiple_value = locators.MULTI_VALUE
    multiple_selector = locators.MULTI_SELECTOR
    multi_value_remove = locators.MULTI_VALUE_REMOVE
    single_input = locators.SINGLE_INPUT
    single_value = locators.SINGLE_VALUE
    single_selector = locators.SINGLE_SELECTOR

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/auto-complete'
        super().__init__(web_driver, url)

    @allure.step('заполнение поля с множеством значений')
    def fill_input_multi(self):
        colors = random.sample(next(generated_color()).color_name, k=random.randint(2, 5))
        for color in colors:
            input_multi = self.multiple_input
            input_multi.send_keys(color)
            self.multiple_selector.click()
        return colors

    @allure.step('проверка цветов в поле с множеством значений')
    def check_color_in_multi(self):
        color_list = self.multiple_value.find_all()
        colors = []
        for color in color_list:
            colors.append(color.text)
        return colors

    @allure.step('Удаление значений из поля с множеством значений')
    def remove_value_from_multi(self):
        count_value_before = self.multiple_value.count()
        remove_button_list = self.multi_value_remove.find_all()
        for value in remove_button_list:
            value.click()
            break
        count_value_after = self.multiple_value.count()
        return count_value_before, count_value_after

    @allure.step('заполнение поля с автозаполнением')
    def fill_input_single(self):
        color = random.sample(next(generated_color()).color_name, k=1)
        input_single = self.single_input
        input_single.send_keys(color[0])
        self.single_selector.click()
        return color[0]

    @allure.step('проверка цвета в поле')
    def check_color_in_single(self):
        color = self.single_value
        return color.get_text()


class SliderPageLocators:
    INPUT_SLIDER = BasePageElement(css_selector='input[class="range-slider range-slider--primary"]')
    SLIDER_VALUE = BasePageElement(css_selector='input[id="sliderValue"]')


class SliderPage(BasePage):
    locators = SliderPageLocators()
    input_slider = locators.INPUT_SLIDER
    slider_value = locators.SLIDER_VALUE

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/slider'
        super().__init__(web_driver, url)


class ProgressBarPageLocators:
    PROGRESS_BAR_BUTTON = BasePageElement(css_selector='button[id="startStopButton"]')
    PROGRESS_BAR_VALUE = BasePageElement(css_selector='div[class="progress-bar bg-info"]')


class ProgressBarPage(BasePage):
    locators = ProgressBarPageLocators()
    progress_bar_button = locators.PROGRESS_BAR_BUTTON
    progress_bar_value = locators.PROGRESS_BAR_VALUE

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/progress-bar'
        super().__init__(web_driver, url)


class TabsPageLocators:
    TABS_WHAT = BasePageElement(css_selector='a[id="demo-tab-what"]')
    TABS_WHAT_CONTENT = BasePageElement(css_selector='div[id="demo-tabpane-what"]')
    TABS_ORIGIN = BasePageElement(css_selector='a[id="demo-tab-origin"]')
    TABS_ORIGIN_CONTENT = BasePageElement(css_selector='div[id="demo-tabpane-origin"]')
    TABS_USE = BasePageElement(css_selector='a[id="demo-tab-use"]')
    TABS_USE_CONTENT = BasePageElement(css_selector='div[id="demo-tabpane-use"]')
    TABS_MORE = BasePageElement(css_selector='a[id="demo-tab-more"]')
    TABS_MORE_CONTENT = BasePageElement(css_selector='div[id="demo-tabpane-more"]')


class TabsPage(BasePage):
    locators = TabsPageLocators()
    what = locators.TABS_WHAT
    what_content = locators.TABS_WHAT_CONTENT
    origin = locators.TABS_ORIGIN
    origin_content = locators.TABS_ORIGIN_CONTENT
    use = locators.TABS_USE
    use_content = locators.TABS_USE_CONTENT
    more = locators.TABS_MORE
    more_content = locators.TABS_MORE_CONTENT

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/tabs'
        super().__init__(web_driver, url)

    @allure.step('check tabs')
    def check_tabs(self, name_tab):
        tabs = {'what':
                    {'title': self.what,
                     'content': self.what_content},
                'origin':
                    {'title': self.origin,
                     'content': self.origin_content},
                'use':
                    {'title': self.use,
                     'content': self.use_content},
                'more':
                    {'title': self.more,
                     'content': self.more_content},
                }

        button = tabs[name_tab]['title']
        button.click()
        time.sleep(0.3)
        tab_content = tabs[name_tab]['content'].get_text()
        return button.get_text(), len(tab_content)


class ToolTipsPageLocators:
    BUTTON = BasePageElement(css_selector='button[id="toolTipButton"]')
    TOOL_TIP_BUTTON = BasePageElement(css_selector='button[aria-describedby="buttonToolTip"]')
    FIELD = BasePageElement(css_selector='input[id="toolTipTextField"]')
    TOOL_TIP_FIELD = BasePageElement(css_selector='input[aria-describedby="textFieldToolTip"]')
    CONTRARY_LINK = BasePageElement(xpath='//*[.="Contrary"]')
    TOOL_TIP_CONTRARY = BasePageElement(css_selector='a[aria-describedby="contraryTexToolTip"]')
    SECTION_LINK = BasePageElement(xpath='//*[.="1.10.32"]')
    TOOL_TIP_SECTION = BasePageElement(css_selector='a[aria-describedby="sectionToolTip"]')
    TOOL_TIPS_INNERS = BasePageElement(css_selector='div[class="tooltip-inner"]')


class ToolTipsPage(BasePage):
    locators = ToolTipsPageLocators()
    button = locators.BUTTON
    tooltip_button = locators.TOOL_TIP_BUTTON
    field = locators.FIELD
    tooltip_field = locators.TOOL_TIP_FIELD
    contrary_link = locators.CONTRARY_LINK
    tool_tip_contrary = locators.TOOL_TIP_CONTRARY
    section_link = locators.SECTION_LINK
    tooltip_section = locators.TOOL_TIP_SECTION
    tooltip_inners = locators.TOOL_TIPS_INNERS

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/tool-tips'
        super().__init__(web_driver, url)

    @allure.step('Получить текст из тултипов')
    def get_text_from_tool_tips(self, hover_elem):
        hover_elem.move_to_element()
        hover_elem.move_to_element()
        time.sleep(0.3)
        text = self.tooltip_inners.get_text()
        return text

    @allure.step('Проверка тултипов')
    def check_tool_tips(self):
        tool_tip_text_button = self.get_text_from_tool_tips(self.button)
        tool_tip_text_field = self.get_text_from_tool_tips(self.field)
        tool_tip_text_contrary = self.get_text_from_tool_tips(self.contrary_link)
        tool_tip_text_section = self.get_text_from_tool_tips(self.section_link)
        return tool_tip_text_button, tool_tip_text_field, tool_tip_text_contrary, tool_tip_text_section

