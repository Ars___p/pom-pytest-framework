import random
from selenium.webdriver import ActionChains
import allure
from pages.base_page import BasePage, BasePageElement, BasePageManyElements


class SortablePageLocators:
    TAB_LIST = BasePageElement(css_selector='a[id="demo-tab-list"]')
    LIST_ITEM = BasePageManyElements(
        css_selector='div[id="demo-tabpane-list"] div[class="list-group-item list-group-item-action"]')
    TAB_GRID = BasePageElement(css_selector='a[id="demo-tab-grid"]')
    GRID_ITEM = BasePageManyElements(
        css_selector='div[id="demo-tabpane-grid"] div[class="list-group-item list-group-item-action"]')


class SortablePage(BasePage):
    locators = SortablePageLocators()
    tab_list = locators.TAB_LIST
    list_item = locators.LIST_ITEM
    tab_grid = locators.TAB_GRID
    grid_item = locators.GRID_ITEM

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/sortable'
        super().__init__(web_driver, url)

    @allure.step('get sortable items')
    def get_sortable_items(self, elements):
        item_list = elements.find_all()
        return [item.text for item in item_list]

    @allure.step('change list order')
    def change_list_order(self):
        self.tab_list.click()
        order_before = self.get_sortable_items(self.list_item)
        item_list = random.sample(self.list_item.find_all(), k=2)
        item_what = item_list[0]
        item_where = item_list[1]
        action = ActionChains(self._web_driver)
        action.drag_and_drop(item_what, item_where).perform()
        order_after = self.get_sortable_items(self.list_item)
        return order_before, order_after

    @allure.step('change grade order')
    def change_grid_order(self):
        self.tab_grid.click()
        order_before = self.get_sortable_items(self.grid_item)
        item_list = random.sample(self.grid_item.find_all(), k=2)
        item_what = item_list[0]
        item_where = item_list[1]
        action = ActionChains(self._web_driver)
        action.drag_and_drop(item_what, item_where).perform()
        order_after = self.get_sortable_items(self.grid_item)
        return order_before, order_after


class SelectablePageLocators:
    TAB_LIST = BasePageElement(css_selector="a[id='demo-tab-list']")
    LIST_ITEM = BasePageManyElements(
        css_selector="ul[id='verticalListContainer'] li[class='mt-2 list-group-item list-group-item-action']")
    LIST_ITEM_ACTIVE = BasePageManyElements(
        css_selector='ul[id="verticalListContainer"] li[class="mt-2 list-group-item active list-group-item-action"]')
    TAB_GRID = BasePageElement(css_selector="a[id='demo-tab-grid']")
    GRID_ITEM = BasePageManyElements(
        css_selector='div[id="gridContainer"]  li[class="list-group-item list-group-item-action"]')
    GRID_ITEM_ACTIVE = BasePageManyElements(
        css_selector='div[id="gridContainer"]  li[class="list-group-item active list-group-item-action"]')


class SelectablePage(BasePage):
    locators = SelectablePageLocators()
    tab_list = locators.TAB_LIST
    list_items = locators.LIST_ITEM
    list_item_active = locators.LIST_ITEM_ACTIVE
    tab_grid = locators.TAB_GRID
    grid_items = locators.GRID_ITEM
    grid_item_active = locators.GRID_ITEM_ACTIVE

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/selectable'
        super().__init__(web_driver, url)

    @allure.step('Клик по элементу из списка')
    def click_selectable_item(self, elements):
        item_list = elements.find_all()
        random.sample(item_list, k=1)[0].click()

    @allure.step('select list item')
    def select_list_item(self):
        self.tab_list.click()
        self.click_selectable_item(self.list_items)
        active_element = self.list_item_active.get_text_of_all_elements()
        return active_element

    @allure.step('select grid item')
    def select_grid_item(self):
        self.tab_grid.click()
        self.click_selectable_item(self.grid_items)
        active_element = self.grid_item_active.get_text_of_all_elements()
        return active_element


class ResizablePageLocators:
    RESIZABLE_BOX_HANDLE = BasePageElement(
        css_selector='div[class="constraint-area"] span[class="react-resizable-handle react-resizable-handle-se"]')
    RESIZABLE_BOX = BasePageElement(css_selector='div[id="resizableBoxWithRestriction"]')
    RESIZABLE_HANDLE = BasePageElement(
        css_selector='div[id="resizable"] span[class="react-resizable-handle react-resizable-handle-se"]')
    RESIZABLE = BasePageElement(css_selector='div[id="resizable"]')


class ResizablePage(BasePage):
    locators = ResizablePageLocators()
    resizable_box_handle = locators.RESIZABLE_BOX_HANDLE
    resizable_box = locators.RESIZABLE_BOX
    resizable_handle = locators.RESIZABLE_HANDLE
    resizable = locators.RESIZABLE

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/resizable'
        super().__init__(web_driver, url)

    @allure.step('Получить высоту и ширину поля')
    def get_px_from_width_height(self, value_of_size):
        width = value_of_size.split(';')[0].split(':')[1].replace(' ', '')
        height = value_of_size.split(';')[1].split(':')[1].replace(' ', '')
        return width, height

    @allure.step('получить минимальные и максимальные размеры')
    def get_max_min_size(self, element):
        size = element.find()
        size_value = size.get_attribute('style')
        return size_value

    @allure.step('Изменить размер поля')
    def change_size_resizable_box(self):
        action = ActionChains(self._web_driver)
        action.move_to_element(self.resizable_box_handle.find()).drag_and_drop_by_offset(
            self.resizable_box_handle.find(), 100, 300).perform()
        max_size = self.get_px_from_width_height(self.get_max_min_size(self.resizable_box))
        action.drag_and_drop_by_offset(self.resizable_box_handle.find(), -500, -300).perform()
        min_size = self.get_px_from_width_height(self.get_max_min_size(self.resizable_box))
        return max_size, min_size

    @allure.step('Изменить размер поля')
    def change_size_resizable(self):
        action = ActionChains(self._web_driver)
        action.move_to_element(self.resizable_box_handle.find()).drag_and_drop_by_offset(
            self.resizable_handle.find(), random.randint(1, 300), random.randint(1, 300)).perform()
        max_size = self.get_px_from_width_height(self.get_max_min_size(self.resizable))
        action.drag_and_drop_by_offset(self.resizable_handle.find(),
                                       random.randint(-200, -1), random.randint(-200, -1)).perform()
        min_size = self.get_px_from_width_height(self.get_max_min_size(self.resizable))
        return max_size, min_size


class DroppablePageLocators:
    # Simple
    SIMPLE_TAB = BasePageElement(css_selector="a[id='droppableExample-tab-simple']")
    DRAG_ME_SIMPLE = BasePageElement(css_selector='div[id="draggable"]')
    DROP_HERE_SIMPLE = BasePageElement(css_selector='#simpleDropContainer #droppable')

    # Accept
    ACCEPT_TAB = BasePageElement(css_selector="a[id='droppableExample-tab-accept']")
    ACCEPTABLE = BasePageElement(css_selector='div[id="acceptable"]')
    NOT_ACCEPTABLE = BasePageElement(css_selector='div[id="notAcceptable"]')
    DROP_HERE_ACCEPT = BasePageElement(css_selector='#acceptDropContainer #droppable')

    # Prevent Propogation
    PREVENT_TAB = BasePageElement(css_selector="a[id='droppableExample-tab-preventPropogation']")
    NOT_GREEDY_DROP_BOX_TEXT = BasePageElement(css_selector='div[id="notGreedyDropBox"] p:nth-child(1)')
    NOT_GREEDY_INNER_BOX = BasePageElement(css_selector='div[id="notGreedyInnerDropBox"]')
    GREEDY_DROP_BOX_TEXT = BasePageElement(css_selector='div[id="greedyDropBox"] p:nth-child(1)')
    GREEDY_INNER_BOX = BasePageElement(css_selector='div[id="greedyDropBoxInner"]')
    DRAG_ME_PREVENT = BasePageElement(css_selector='#ppDropContainer #dragBox')

    # Revert Draggable
    REVERT_TAB = BasePageElement(css_selector="a[id='droppableExample-tab-revertable']")
    WILL_REVERT = BasePageElement(css_selector='div[id="revertable"]')
    NOT_REVERT = BasePageElement(css_selector='div[id="notRevertable"]')
    DROP_HERE_REVERT = BasePageElement(css_selector='#revertableDropContainer #droppable')


class DroppablePage(BasePage):
    locators = DroppablePageLocators()
    simple_tab = locators.SIMPLE_TAB
    drag_me_simple = locators.DRAG_ME_SIMPLE
    drop_here_simple = locators.DROP_HERE_SIMPLE
    accept_tab = locators.ACCEPT_TAB
    acceptable = locators.ACCEPTABLE
    not_acceptable = locators.NOT_ACCEPTABLE
    drop_here_accept = locators.DROP_HERE_ACCEPT
    prevent_tab = locators.PREVENT_TAB
    not_greedy_drop_box_text = locators.NOT_GREEDY_DROP_BOX_TEXT
    not_greedy_inner_box = locators.NOT_GREEDY_INNER_BOX
    greedy_inner_box = locators.GREEDY_INNER_BOX
    drag_me_prevent = locators.DRAG_ME_PREVENT
    revert_tab = locators.REVERT_TAB
    will_revert = locators.WILL_REVERT
    not_revert = locators.NOT_REVERT
    drop_here_revert = locators.DROP_HERE_REVERT

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/droppable'
        super().__init__(web_driver, url)
