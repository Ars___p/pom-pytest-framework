import random
import time
import allure
import requests

from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from pages.base_page import BasePage, BasePageElement, BasePageManyElements
from generator.generator import generated_person


# Функция обращается к генератору и возвращает необходимые для тестов значения
@allure.step("Сгенерировать человека")
def generate_person():
    person_info = next(generated_person())
    firstname = person_info.firstname
    lastname = person_info.lastname
    full_name = person_info.full_name
    email = person_info.email
    age = person_info.age
    salary = person_info.salary
    department = person_info.department
    current_address = person_info.current_address
    permanent_address = person_info.permanent_address
    return [firstname, lastname, full_name, email, str(age), str(salary),
            department, current_address, permanent_address]


class TextBoxPageLocators:
    # Поля для ввода
    FULL_NAME = BasePageElement(css_selector="input[id='userName']")
    EMAIL = BasePageElement(css_selector='input[id="userEmail"]')
    CURRENT_ADDRESS = BasePageElement(css_selector='textarea[id="currentAddress"]')
    PERMANENT_ADDRESS = BasePageElement(css_selector='textarea[id="permanentAddress"]')
    SUBMIT = BasePageElement(css_selector='button[id="submit"]')

    # Поля на выходе
    CREATED_FULL_NAME = BasePageElement(css_selector='#output #name')
    CREATED_EMAIL = BasePageElement(css_selector='#output #email')
    CREATED_CURRENT_ADDRESS = BasePageElement(css_selector='#output #currentAddress')
    CREATED_PERMANENT_ADDRESS = BasePageElement(css_selector='#output #permanentAddress')


class TextBoxPage(BasePage):
    locators = TextBoxPageLocators

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/text-box'
        super().__init__(web_driver, url)

    full_name = locators.FULL_NAME
    email = locators.EMAIL
    current_address = locators.CURRENT_ADDRESS
    permanent_address = locators.PERMANENT_ADDRESS
    submit = locators.SUBMIT
    created_full_name = locators.CREATED_FULL_NAME
    created_email = locators.CREATED_EMAIL
    created_current_address = locators.CREATED_CURRENT_ADDRESS
    created_permanent_address = locators.CREATED_PERMANENT_ADDRESS


class CheckBoxLocators:
    EXPAND_ALL_BUTTON = BasePageElement(css_selector="button[title='Expand all']")
    ITEM_LIST = BasePageManyElements(css_selector="span[class='rct-title']")
    CHECKED_ITEMS = BasePageManyElements(css_selector="svg[class='rct-icon rct-icon-check']")
    TITLE_ITEM = BasePageElement(xpath=".//ancestor::span[@class='rct-text']")
    OUTPUT_RESULT = BasePageManyElements(css_selector="span[class='text-success']")


class CheckBoxPage(BasePage):
    locators = CheckBoxLocators

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/checkbox'
        super().__init__(web_driver, url)

    # Рандомно активируем чекбоксы исключая папку HOME
    @allure.step("Активировать случайные чекбоксы")
    def click_random_checkbox(self):
        item_list = self.locators.ITEM_LIST.find_all()
        count = 17
        while count != 0:
            item = item_list[random.randint(1, 16)]
            if count > 0:
                self.scroll_down()
                item.click()
                count -= 1
            else:
                break

    # Функция возвращает список активированных чекбоксов в удобном для сравнения виде
    @allure.step("Получить список активированных чекбоксов")
    def get_checked_checkboxes(self):
        checked_list = self.locators.CHECKED_ITEMS.find_all()
        data = []
        for box in checked_list:
            title_item = box.find_element(By.XPATH, ".//ancestor::span[@class='rct-text']")
            data.append(title_item.text)
        return str(data).replace(' ', '').replace('doc', '').replace('.', '').lower()

    # Функция возвращает в удобном для сравнения виде список чекбоксов в поле на выходе
    @allure.step("Получить итоговый результат")
    def get_output_result(self):
        result_list = self.locators.OUTPUT_RESULT.find_all()
        data = []
        for item in result_list:
            data.append(item.text)
        return str(data).replace(' ', '').lower()

    expand_all = locators.EXPAND_ALL_BUTTON
    item_list = locators.ITEM_LIST
    checked_list = locators.CHECKED_ITEMS
    result_list = locators.OUTPUT_RESULT


class RadioButtonsLocators:
    YES_RADIOBUTTON = BasePageElement(css_selector='label[class^="custom-control"][for="yesRadio"]')
    IMPRESSIVE_RADIOBUTTON = BasePageElement(css_selector='label[class^="custom-control"][for="impressiveRadio"]')
    NO_RADIOBUTTON = BasePageElement(css_selector='label[class^="custom-control"][for="noRadio"]')
    OUTPUT_RESULT = BasePageElement(css_selector='p span[class="text-success"]')


class RadioButtonPage(BasePage):
    locators = RadioButtonsLocators()

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/radio-button'
        super().__init__(web_driver, url)

    yes_radiobutton = locators.YES_RADIOBUTTON
    impressive_radiobutton = locators.IMPRESSIVE_RADIOBUTTON
    no_radiobutton = locators.NO_RADIOBUTTON
    output = locators.OUTPUT_RESULT


class WebTableLocators:
    # Форма добавления карточки
    ADD_BUTTON = BasePageElement(css_selector='button[id="addNewRecordButton"]')
    FIRSTNAME_INPUT = BasePageElement(css_selector='input[id="firstName"]')
    LASTNAME_INPUT = BasePageElement(css_selector='input[id="lastName"]')
    EMAIL_INPUT = BasePageElement(css_selector='input[id="userEmail"]')
    AGE_INPUT = BasePageElement(css_selector='input[id="age"]')
    SALARY_INPUT = BasePageElement(css_selector='input[id="salary"]')
    DEPARTMENT_INPUT = BasePageElement(css_selector='input[id="department"]')
    SUBMIT = BasePageElement(css_selector='button[id="submit"]')

    # Таблица
    FULL_PEOPLE_LIST = BasePageManyElements(css_selector="div[class='rt-tr-group']")
    SEARCH_INPUT = BasePageElement(css_selector='input[id="searchBox"]')
    DELETE_BUTTON = BasePageElement(css_selector='span[title="Delete"]')
    ROW_PARENT = ".//ancestor::div[@class='rt-tr-group']"
    NO_ROWS_FOUND = BasePageElement(css_selector='div[class="rt-noData"]')
    COUNT_ROW_LIST = BasePageElement(css_selector='select[aria-label="rows per page"]')

    # Обновление
    UPDATE_BUTTON = BasePageElement(css_selector='span[title="Edit"]')


class WebTablePage(BasePage):
    locators = WebTableLocators()

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/webtables'
        super().__init__(web_driver, url)

    add_button = locators.ADD_BUTTON
    first_name_input = locators.FIRSTNAME_INPUT
    last_name_input = locators.LASTNAME_INPUT
    email_input = locators.EMAIL_INPUT
    age_input = locators.AGE_INPUT
    salary_input = locators.SALARY_INPUT
    department_input = locators.DEPARTMENT_INPUT
    submit_button = locators.SUBMIT
    full_list = locators.FULL_PEOPLE_LIST
    delete_button = locators.DELETE_BUTTON
    search_input = locators.SEARCH_INPUT
    update_button = locators.UPDATE_BUTTON
    no_rows_found = locators.NO_ROWS_FOUND
    count_rows_list = locators.COUNT_ROW_LIST

    @allure.step("Проверить добавление нового человека в таблицу")
    def check_new_added_person(self):
        people_list = self.locators.FULL_PEOPLE_LIST.find_all()
        data = []
        for item in people_list:
            data.append(item.text.splitlines())
        return data

    @allure.step("Проверить поиск человека в таблице")
    def check_search_person(self):
        delete_button = self.locators.DELETE_BUTTON.find()
        row = delete_button.find_element(By.XPATH, ".//ancestor::div[@class='rt-tr-group']")
        return row.text.splitlines()

    @allure.step("Обновить провессию человека")
    def update_person_department(self):
        person_info = next(generated_person())
        department = person_info.department
        self.locators.DEPARTMENT_INPUT.send_keys(department)
        return department


class ButtonsPageLocators:
    DOUBLE_CLICK = BasePageElement(css_selector="button[id='doubleClickBtn']")
    DOUBLE_CLICK_MESSAGE = BasePageElement(id="doubleClickMessage")
    RIGHT_CLICK_BUTTON = BasePageElement(css_selector="button[id='rightClickBtn']")
    RIGHT_CLICK_MESSAGE = BasePageElement(id="rightClickMessage")
    CLICK_ME_BUTTON = BasePageElement(xpath="//div[3]/button")
    CLICK_ME_MESSAGE = BasePageElement(id="dynamicClickMessage")


class ButtonsPage(BasePage):
    locators = ButtonsPageLocators()

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/buttons'
        super().__init__(web_driver, url)

    double_click_button = locators.DOUBLE_CLICK
    double_click_message = locators.DOUBLE_CLICK_MESSAGE
    right_click_button = locators.RIGHT_CLICK_BUTTON
    right_click_message = locators.RIGHT_CLICK_MESSAGE
    click_me_button = locators.CLICK_ME_BUTTON
    click_me_message = locators.CLICK_ME_MESSAGE


class LinksPageLocators:
    VALID_LINK = BasePageElement(css_selector="a[href='http://demoqa.com']")
    BROKEN_LINK = BasePageElement(css_selector="a[href='http://the-internet.herokuapp.com/status_codes/500']")
    VALID_IMAGE = BasePageElement(css_selector='p + img[src="/images/Toolsqa.jpg"]')
    BROKEN_IMAGE = BasePageElement(css_selector='p + img[src="/images/Toolsqa_1.jpg"]')


class LinksPage(BasePage):
    locators = LinksPageLocators()

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/broken'
        super().__init__(web_driver, url)

    valid_link = locators.VALID_LINK
    broken_link = locators.BROKEN_LINK
    valid_image = locators.VALID_IMAGE
    broken_image = locators.BROKEN_IMAGE

    @allure.step("Проверить ссылку")
    def check_link(self, choice):
        choices = {'valid': self.valid_link,
                   'broken': self.broken_link}

        link = choices[choice]
        link_href = link.get_attribute('href')
        request = requests.get(link_href)
        link.click()
        try:
            self.switch_window(1)
        finally:
            current_url = self.get_current_url()
            return request.status_code, current_url, link_href

    @allure.step("Проверить изображение")
    def check_image(self, choice):
        choices = {'valid': 'https://demoqa.com/images/Toolsqa.jpg',
                   'broken': 'https://demoqa.com/images/Toolsqa_1.jpg'}
        image = choices[choice]
        request = requests.get(image)
        return request.status_code


class DynamicPropertiesLocators:
    COLOR_CHANGE_BUTTON = BasePageElement(css_selector='button[id="colorChange"]')
    VISIBLE_AFTER_FIVE_SEC_BUTTON = BasePageElement(css_selector='button[id="visibleAfter"]')
    ENABLE_BUTTON = BasePageElement(css_selector='button[id="enableAfter"]')


class DynamicPropertiesPage(BasePage):
    locators = DynamicPropertiesLocators()

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'https://demoqa.com/dynamic-properties'
        super().__init__(web_driver, url)

    color_change_button = locators.COLOR_CHANGE_BUTTON
    visible_after_five_sec_button = locators.VISIBLE_AFTER_FIVE_SEC_BUTTON
    enable_button = locators.ENABLE_BUTTON

    @allure.step('Проверка активации кнопки')
    def check_enable_button(self):
        try:
            self.enable_button.wait_to_be_clickable()
        except TimeoutException:
            return False
        return True

    @allure.step('Проверка появления кнопки')
    def check_appear_of_button(self):
        try:
            self.visible_after_five_sec_button.wait_until_not_visible()
        except TimeoutException:
            return False
        return True

    @allure.step('Проверка смены цвета')
    def check_changed_of_color(self):
        color_button = self.color_change_button
        color_button_before = color_button.get_attribute('class')
        time.sleep(5)
        color_button_after = color_button.get_attribute('class')
        return color_button_before, color_button_after
