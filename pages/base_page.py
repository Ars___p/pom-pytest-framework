import time
import uuid
import allure

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class BasePage:
    _webdriver = None

    def __init__(self, driver, url=''):
        self._web_driver = driver
        self.get(url)

    def __setattr__(self, name, value):
        if not name.startswith('_'):
            self.__getattribute__(name)._set_value(self._web_driver, value)
        else:
            super(BasePage, self).__setattr__(name, value)

    def __getattribute__(self, item):
        attr = object.__getattribute__(self, item)

        if not item.startswith('_') and not callable(attr):
            attr._web_driver = self._web_driver
            attr._page = self

        return attr

    def get(self, url):
        self._web_driver.get(url)

    def go_back(self):
        self._web_driver.back()

    def refresh(self):
        self._web_driver.refresh()

    def screenshot(self, file_name='screenshots/' + str(uuid.uuid4()) + '.png'):
        self._web_driver.save_screenshot(file_name)

    @allure.step('Скролл страницы вниз.')
    def scroll_down(self, offset=0):
        """ Скролл страницы вниз. """

        if offset:
            self._web_driver.execute_script('window.scrollTo(0, {0});'.format(offset))
        else:
            self._web_driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')

    @allure.step('Скролл страницы вверх.')
    def scroll_up(self, offset=0):
        """ Скролл страницы вверх. """

        if offset:
            self._web_driver.execute_script('window.scrollTo(0, -{0});'.format(offset))
        else:
            self._web_driver.execute_script('window.scrollTo(0, -document.body.scrollHeight);')

    @allure.step('Сменить активкую вкладку браузера.')
    def switch_window(self, number):
        f""" Сменить вкладку браузера на {number}. """

        self._web_driver.switch_to.window(self._web_driver.window_handles[number])

    @allure.step('Переключиться на alert')
    def switch_to_alert(self):
        """ Переключиться на alert. """

        return self._web_driver.switch_to.alert

    @allure.step('Переключиться на iframe')
    def switch_to_iframe(self, iframe):
        """ Переключиться на iframe по его имени. """

        self._web_driver.switch_to.frame(iframe)

    @allure.step('Выйти из элемента iframe')
    def switch_out_iframe(self):
        """ Выйти из элемента iframe. """
        self._web_driver.switch_to.default_content()

    @allure.step('Вернуть URL из адресной строки.')
    def get_current_url(self):
        """ Вернуть URL из адресной строки.  """

        return self._web_driver.current_url

    @allure.step('Получить исходник страницы.')
    def get_page_source(self):

        source = ''
        try:
            source = self._web_driver.page_source
        except:
            print('Не удается получить исходник страницы')

        return source

    @allure.step('Проверка наличия JS ошибок в консоли.')
    def check_js_errors(self, ignore_list=None):
        """ Проверка наличия JS ошибок в консоли, на данной странице. """

        ignore_list = ignore_list or []

        logs = self._web_driver.get_log('browser')
        for log_message in logs:
            if log_message['level'] != 'WARNING':
                ignore = False
                for issue in ignore_list:
                    if issue in log_message['message']:
                        ignore = True
                        break

                assert ignore, 'JS error "{0}" on the page!'.format(log_message)

    @allure.step('Ожидать полной загрузки страницы.')
    def wait_page_loaded(self, timeout=60, check_js_complete=True,
                         check_page_changes=False, check_images=False,
                         wait_for_element=None,
                         wait_for_xpath_to_disappear='',
                         sleep_time=2):
        """ Эта функция ожидает полной загрузки страницы
            Для этого используется несколько способов определить загрузилась ли страница:

            1) Проверка статуса JS
            2) Проверка изменяется ли DOM-дерево страницы
            3) Проверка загрузились ли все изображения на странице
               (P.S. Отключена по умолчанию)
            4) Проверка, что заданные элементы появились на странице
        """

        page_loaded = False
        double_check = False
        k = 0

        if sleep_time:
            time.sleep(sleep_time)

        # Получить DOM-дерево страницы, для отслеживания изменений в HTML:
        source = ''
        try:
            source = self._web_driver.page_source
        except:
            pass

        # Ожидание полной загрузки страницы:
        while not page_loaded:
            time.sleep(2)
            k += 1

            if check_js_complete:
                # Скролл вниз и ожидание полной загрузки страницы (для обхода Lazy Load):
                try:
                    self._web_driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')
                    page_loaded = self._web_driver.execute_script("return document.readyState == 'complete';")
                except Exception as e:
                    pass

            if page_loaded and check_page_changes:
                # Прверка изменений в структуре страницы
                new_source = ''
                try:
                    new_source = self._web_driver.page_source
                except:
                    pass

                page_loaded = new_source == source
                source = new_source

            # Ожидание, пока ненужный элемент пропадет со страницы (например индикатор загрузки):
            if page_loaded and wait_for_xpath_to_disappear:
                bad_element = None

                try:
                    bad_element = WebDriverWait(self._web_driver, 0.1).until(
                        EC.presence_of_element_located((By.XPATH, wait_for_xpath_to_disappear))
                    )
                except:
                    pass  # Пропуск ошибок таймаута

                page_loaded = not bad_element
            # Ожидание, появления необходимого элемента на странице:
            if page_loaded and wait_for_element:
                try:
                    page_loaded = WebDriverWait(self._web_driver, 0.1).until(
                        EC.element_to_be_clickable(wait_for_element._locator)
                    )
                except:
                    pass  # Пропуск ошибок таймаут

            assert k < timeout, 'The page loaded more than {0} seconds!'.format(timeout)

            if page_loaded and not double_check:
                page_loaded = False
                double_check = True

        # Скролл страницы обратно наверх:
        self._web_driver.execute_script('window.scrollTo(document.body.scrollHeight, 0);')


class BasePageElement:
    _web_driver = None
    _locator = ('', '')
    _page = None
    _timeout = 10

    def __init__(self, timeout=10, **kwargs):
        self._timeout = timeout

        for attr in kwargs:
            self._locator = (str(attr).replace('_', ' '), str(kwargs.get(attr)))

    @allure.step('Поиск элемента на странице.')
    def find(self, timeout=10):
        """ Поиск элемента на странице. """

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.presence_of_element_located(self._locator)
            )
        except:
            print('Элемент не найден на странице!')

        return element

    @allure.step('Ожидание, пока элемент не станет кликабельным.')
    def wait_to_be_clickable(self, timeout=10, check_visibility=True):
        """ Ожидание, пока элемент не станет кликабельным. """

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.element_to_be_clickable(self._locator)
            )
        except:
            print('Элемент не кликабелен!"')

        if check_visibility:
            self.wait_until_not_visible()

        return element

    @allure.step('Проверка элемента на кликабельность.')
    def is_clickable(self):
        """ Проверка элемента на кликабельность. """

        element = self.wait_to_be_clickable()
        return element is not None

    @allure.step('Проверка наличия элемента на странице.')
    def is_presented(self):
        """ Проверка наличия элемента на странице. """

        element = self.find()
        return element is not None

    @allure.step('Проверка на видимость элемента.')
    def is_visible(self):
        """ Проверка на видимость элемента. """

        element = self.find()

        if element:
            return element.is_displayed()

        return False

    @allure.step('Ожидание пока элемент не исчезнет со страницы')
    def wait_until_not_visible(self, timeout=10):

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.visibility_of_element_located(self._locator)
            )
        except:
            print('Элемент не виден на странице!')

        if element:
            js = ('return (!(arguments[0].offsetParent === null) && '
                  '!(window.getComputedStyle(arguments[0]) === "none") &&'
                  'arguments[0].offsetWidth > 0 && arguments[0].offsetHeight > 0'
                  ');')
            visibility = self._web_driver.execute_script(js, element)
            iteration = 0

            while not visibility and iteration < 10:
                time.sleep(2)

                iteration += 1

                visibility = self._web_driver.execute_script(js, element)
                print('Элемент {0} виден: {1}'.format(self._locator, visibility))

        return element

    @allure.step('Нажатие клавиш в элемент.')
    def send_keys(self, keys, wait=1):
        """ Нажатие клавиш в элемент. """

        keys = keys.replace('\n', '\ue007')

        element = self.find()

        if element:
            element.click()
            element.clear()
            element.send_keys(keys)
            time.sleep(wait)
        else:
            msg = 'Элемент с локатором {0} не найден'
            raise AttributeError(msg.format(self._locator))

    @allure.step('Получить текст элемента.')
    def get_text(self):
        """ Получить текст элемента. """

        element = self.find()
        text = ''

        try:
            text = str(element.text)
        except Exception as e:
            print('Ошибка: {0}'.format(e))

        return text

    @allure.step('Получить атрибут элемента.')
    def get_attribute(self, attr_name):
        """ Получить атрибут элемента. """

        element = self.find()

        if element:
            return element.get_attribute(attr_name)

    @allure.step('Кликнуть на элемент.')
    def click(self, hold_seconds=0, x_offset=1, y_offset=1):
        """ Кликнуть на элемент. """

        element = self.wait_to_be_clickable()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element_with_offset(element, x_offset, y_offset). \
                pause(hold_seconds).click(on_element=element).perform()
        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))

    @allure.step('Клик правой кнопки мыши на элемент.')
    def right_mouse_click(self, x_offset=0, y_offset=0, hold_seconds=0):
        """ Клик правой кнопки мыши на элемент. """

        element = self.wait_to_be_clickable()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element_with_offset(element, x_offset, y_offset). \
                pause(hold_seconds).context_click(on_element=element).perform()
        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))

    @allure.step('Двойной клик на элемент.')
    def double_click(self, x_offset=0, y_offset=0, hold_seconds=0):
        """ Двойной клик на элемент. """

        element = self.wait_to_be_clickable()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element_with_offset(element, x_offset, y_offset). \
                pause(hold_seconds).double_click(on_element=element).perform()
        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))

    @allure.step('Наведение курсора на элемент')
    def move_to_element(self, hold_seconds=0):
        """ Наведение курсора на элемент. """

        element = self.find()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element(element). \
                pause(hold_seconds).perform()

        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))

    @allure.step('Перетащить элемент по координатам')
    def drag_and_drop_by_offset(self, x_offset=0, y_offset=0):
        """ Перетащить элемент по координатам. """

        element = self.find()

        if element:
            action = ActionChains(self._web_driver)
            action.drag_and_drop_by_offset(element, x_offset, y_offset).perform()

        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))

    @allure.step('Перетащить элемент на другой')
    def drag_and_drop_to_element(self, target_element):
        """ Перетащить элемент на другой. """

        element = self.find()
        target_element = target_element.find()

        if element:
            action = ActionChains(self._web_driver)
            action.drag_and_drop(element, target_element).perform()

        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))


    @allure.step('Выделить и сделать скриншот элемента.')
    def highlight_and_make_screenshot(self, file_name='screenshots/' + str(uuid.uuid4()) + '.png'):
        """ Выделить и сделать скриншот элемента. """

        element = self.find()

        # Скролл до элемента:
        self._web_driver.execute_script("arguments[0].scrollIntoView();", element)

        # Добавить красную обводку:
        self._web_driver.execute_script("arguments[0].style.border='3px solid red'", element)

        # Сделать скриншот страницы:
        self._web_driver.save_screenshot(file_name)

    @allure.step('Скролл до элемента.')
    def scroll_to_element(self):
        """ Скролл до элемента. """

        element = self.find()
        self._web_driver.execute_script("arguments[0].scrollIntoView();", element)

        # Другой способ проскролить до элемента:
        #try:
        #    element.send_keys(Keys.DOWN)
        #except Exception as e:
        #    pass  # Игнорирование ошибки связанной с нажатием клавиши вниз в элемент

    @allure.step('Удалить элемент со страницы с помощью JS.')
    def delete(self):
        """ Удалить элемент со страницы с помощью JS. """

        element = self.find()

        self._web_driver.execute_script("arguments[0].remove();", element)


class BasePageManyElements(BasePageElement):

    def __getitem__(self, item):
        """ Получить список элементов, попытка вернуть требуемый элемент. """

        elements = self.find()
        return elements[item]

    @allure.step('Поиск элементов на странице.')
    def find_all(self, timeout=10):
        """ Поиск элементов на странице. """

        elements = []

        try:
            elements = WebDriverWait(self._web_driver, timeout).until(
                EC.presence_of_all_elements_located(self._locator)
            )
        except:
            print('Элементы не найдены на странице!')

        return elements

    @allure.step('Получить количество элементов.')
    def count(self):
        """ Получить количество элементов. """

        elements = self.find_all()
        return len(elements)

    @allure.step('Получить текст элементов.')
    def get_text_of_all_elements(self):
        """ Получить текст элементов. """

        elements = self.find_all()
        result = []

        for element in elements:
            text = ''

            try:
                text = str(element.text)
            except Exception as e:
                print('Ошибка: {0}'.format(e))

            result.append(text)

        return result

    @allure.step('Получить атрибут элементов.')
    def get_attribute_of_all_elements(self, attr_name):
        """ Получить атрибут элементов. """

        results = []
        elements = self.find_all()

        for element in elements:
            results.append(element.get_attribute(attr_name))

        return results

    @allure.step('Выделить элементы и сделать скриншот всей страницы')
    def highlight_and_make_screenshot_of_all_elements(self, file_name='screenshots/' + str(uuid.uuid4()) + '.png'):
        """ Выделить элементы и сделать скриншот всей страницы. """

        elements = self.find_all()

        for element in elements:
            # Скролл до элемента:
            self._web_driver.execute_script("arguments[0].scrollIntoView();", element)

            # Добавить красную обводку:
            self._web_driver.execute_script("arguments[0].style.border='3px solid red'", element)

        # Сделать скриншот:
        self._web_driver.save_screenshot(file_name)
